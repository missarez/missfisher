   var items = $('.galleryImages .item img');
   var currentItem = 0;
   var totalItems = items.length;

   if ($(window).width() < 580) {
       var imageWidth = 300;

   }
   if ($(window).width() > 580) {

       var imageWidth = 500;
   }
   $(window).resize(function() {
       if ($(window).width() < 580) {
           imageWidth = 300;

       }
       if ($(window).width() > 580) {
           imageWidth = 500;


       }
   });


   $('.galleryImages').width(totalItems * imageWidth);

   function prevImage() {

       currentItem--;
       if (currentItem <= 0) {
           currentItem = 0;
       }

       slideNextImage(currentItem);
   }

   function nextImage() {

       currentItem++;
       if (currentItem >= totalItems) {
           currentItem = totalItems - 1;
       }

       slideNextImage(currentItem);
   }

   function slideNextImage(pos) {
       var px = imageWidth * pos * -1;
       $('.galleryImages').animate({
           left: px
       }, 300);
   }



   $('.next').on('click', nextImage);
   $('.previous').on('click', prevImage);