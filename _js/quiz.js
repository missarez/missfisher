//user clicks on btn
const takethequiz = $('.quiz button');
const arrayQuestions = $('.quiz form div');
const answers = [];
var i = 0;

//put form elements in an array
const questions = $.map(arrayQuestions, function(i, val) {
    return i;
})

//initiate questions
var vasso = $('.quiz form').html(questions[i])
//set the answer correct to compare
var target = 'correct';


//when change radio
var checkAnswer = function() {
	//load next question
    i++;
    //take the value
    var answer = $("input:checked").val();

    //store value  in array

    if (i <= questions.length) {

        $('.quiz form').html(questions[i])
        if (answer === 'correct') {
            answers.push(answer)        
        } else if (answer === 'wrong') {
            answers.push(answer)

        }
    }
//get the length of the correct answers
    var numOccurences = $.grep(answers, function(elem) {
        return elem === target;
    }).length;


//show results of correct answers
    if (i === questions.length) {
        $('.quiz form').html("You have answered " + numOccurences + " out of " + questions.length + " correct");
    }


}


//opens a lightbox
var openQuiz = function() {

        $('.quizFormat').css({
            display: 'block'
        }).animate({
            opacity: 1
        }, 300)
    }
    //closes a lightbox
var closeQuiz = function() {
    $('.quizFormat').animate({
        opacity: 0
    }, 300).css({
        display: 'none'
    })
}

 


takethequiz.on('click', openQuiz);
$('.close').on('click', closeQuiz);
vasso.on('change', checkAnswer);