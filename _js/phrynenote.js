//click button

var $gobtn = $('#gobtn');
var $inputName = $('#name');
var $inputEmail = $('#email');
var $inputNameValue = $inputName.val();
var $inputEmailValue = $inputEmail.val;

var $submit = $('#submit');
// keep the placeholder value on the holder key with data
var $placeholderName = $('#name').data('holderName', $('#name').attr('placeholder'));
var $placeholderEmail = $('#email').data('holderName', $('#email').attr('placeholder'));

//animate the panel
function panelRotate() {
    $(".note-wrapper").css({
        //animate pane
        transform: 'rotateY(180deg)'
    });
}

//remove placeholder on click
function removePlaceholder() {
    $(this).attr('placeholder', '')
}
//put back placeholder on focusout

function putPlaceholderback() {
    var data = $(this).data('holderName');
    $(this).attr('placeholder', data);
    $(this).next().css({
        opacity: 0,
        transition: 'opacity .5s'
    });
}


function rightValues() {
    if ($(this).val().length === 0) {
        $(this).next().css({
            opacity: 1,
            transition: 'opacity .5s'
        });
    } else {
        $(this).next().css({
            opacity: 0,
            transition: 'opacity .5s'
        });
    }

}
//check if value is empty 
//check if email entered
//not value enetred show message
//if not email correct show message
//if both rules are ok submit

function submitted(e) {
    e.preventDefault();
    console.log('entered');

    var formInput = $('form').serialize();
    $.post("upload.php", formInput, function(data) {

        $('#status').html(data);

        if ($('#status').html(data).text() == 'Thank you for registering') {
            console.log('thank you');

            $(".note-wrapper").css({
                transform: 'rotateY(0)'
            });

            $inputName.val('');
            $inputEmail.val('');
        }
    })
}

$gobtn.click(panelRotate);
$inputName.focusin(removePlaceholder).focusout(putPlaceholderback).focus(rightValues).keyup(rightValues);
$inputEmail.focusin(removePlaceholder).focusout(putPlaceholderback).focus(rightValues).keyup(rightValues);
$submit.on('click', submitted);